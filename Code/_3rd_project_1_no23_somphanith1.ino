#include <Keypad.h>
#include <IRremote.h>
#include <LiquidCrystal_I2C.h>
#define R 12
#define G 13

LiquidCrystal_I2C lcd(0x27, 16, 2);

int button = 0;
int page = 0;
int price = 0;
const int PIEZO = 11;

int mapCodeToButton(unsigned long code) {
  if ((code & 0x0000FFFF) == 0x0000BF00) {
    code >>= 16;
    if (((code >> 8) ^ (code & 0x00FF)) == 0x00FF) {
      int buttonValue = code & 0xFF;
      switch (buttonValue) {
        case 12: return 0;
        case 16: return 1;
        case 17: return 2;
        case 18: return 3;
        case 20: return 4;
        case 21: return 5;
        case 22: return 6;
        case 24: return 7;
        case 25: return 8;
        case 26: return 9;
        case 0: return 10;  // Negative sign indicator
        case 1: return 11;  // Decimal point indicator
        case 2: return 12;  // Enter to print "Correct"
        default: return -1;
      }
    }
  }
  return -1;
}

int readInfrared() {
  if (IrReceiver.decode()) {
    unsigned long code = IrReceiver.decodedIRData.decodedRawData;
    int result = mapCodeToButton(code);
    IrReceiver.resume();
    return result;
  }
  return -1;
}

// Keypad setup
const byte ROWS = 4;
const byte COLS = 4;
char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
byte rowPins[ROWS] = {10, 9, 8, 7};
byte colPins[COLS] = {6, 5, 4, 3};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

void setup() {
  char key = 'z';
  page = 0;
  lcd.init();
  lcd.clear();
  lcd.backlight();
  pinMode(11, OUTPUT);
  Serial.begin(9600);
  IrReceiver.begin(2);
  displayWelcome();
    pinMode(R, OUTPUT);	//Configuração dos pinos como saída
  	pinMode(G, OUTPUT);
}

void displayWelcome() {
   analogWrite(R, 0); //Ligando as portas PWM com valor aleatório de duty cicle
    analogWrite(G, 0);
  lcd.setCursor(0, 0);
  lcd.print("    welcome  ");
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("YourChoice: ");
  lcd.setCursor(0, 1);
  lcd.print(" 1, 2, 3  ");
  delay(1000);
  displayChoice();
}

void displayChoice() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("YourChoice: ");
  lcd.setCursor(0, 1);
  lcd.print(" 1 is beverage");
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("YourChoice: ");
  lcd.setCursor(0, 1);
  lcd.print(" 2 is snack");
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("YourChoice: ");
  lcd.setCursor(0, 1);
  lcd.print(" 3 is All Other");
  delay(1000);
    lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("YourChoice: ");
  lcd.setCursor(0, 1);
  lcd.print(" 1, 2, 3  ");
  delay(1000);
}

void displayMenu(const char* type, const char* items[]) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(type);
  lcd.setCursor(0, 1);
  lcd.print(" 1, 2, 3  ");
  delay(1000);
  for (int i = 0; i < 3; i++) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(type);
    lcd.setCursor(0, 1);
    lcd.print(items[i]);
    delay(1000);
  }
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(type);
  lcd.setCursor(0, 1);
  lcd.print(" 1, 2, 3  ");
  delay(1000);
}

void displayPrice(const char* item, int priceValue) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(item);
  lcd.setCursor(0, 1);
  lcd.print("price: ");
  lcd.print(priceValue);
  delay(1500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("price: ");
  lcd.print(priceValue);
  lcd.setCursor(0, 1);
  lcd.print("Push A KeyToBuy");
  price = priceValue;
   delay(500);
}

int getAmount() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Enter amount: ");
  delay(1000);

  int enteredValue = 0;
  bool inputComplete = false;

  while (!inputComplete) {
    button = readInfrared();
    if (button >= 0 && button <= 9) {
      enteredValue = enteredValue * 10 + button;
      lcd.setCursor(0, 1);
      lcd.print("Amount: ");
      lcd.print(enteredValue);
    } else if (button == 12) {
      inputComplete = true;
    }
  }
  return enteredValue;
}

void confirmPurchase(int enteredValue) {
  lcd.clear();
  lcd.setCursor(0, 0);
  if (enteredValue == price) {
    lcd.print("buy confirm");
       analogWrite(R, 0); //Ligando as portas PWM com valor aleatório de duty cicle
    analogWrite(G, 255);

  } else {
    lcd.print("wrong amount");
       analogWrite(R, 255); //Ligando as portas PWM com valor aleatório de duty cicle
    analogWrite(G, 0);

  }
  delay(2000);
  setup();
}

void loop() {
  char key = keypad.getKey();
  button = readInfrared();

  if (key || button >= 0) {
     digitalWrite(11, HIGH);
     delay(50);
    digitalWrite(11, LOW);
    Serial.println(key);
    Serial.println(button);

    if (page == 0) {
      if (key == '1') {
        page = 1;
        const char* beverages[] = {"1 is Pepsi", "2 is oishi", "3 is soda"};
        displayMenu("beverage: ", beverages);
      } else if (key == '2') {
        page = 2;
        const char* snacks[] = {"1 is cookie", "2 is chip", "3 is gummy"};
        displayMenu("Snack: ", snacks);
      } else if (key == '3') {
        page = 3;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("YourChoice: ");
        lcd.setCursor(0, 1);
        lcd.print(" 3  ");
        delay(1000);
        setup();
      }
    } else if (page == 1 || page == 2) {
      const char* items[] = {
        page == 1 ? "beverage: Pepsi" : "Snack: cookie",
        page == 1 ? "beverage: oishi" : "Snack: chip",
        page == 1 ? "beverage: soda" : "Snack: gummy"
      };
     /* if (key == 'A') {
        int enteredValue = getAmount();
        confirmPurchase(enteredValue);
      }*/
      displayPrice(items[key - '1'], 2000);
      if (key == 'A') {
        int enteredValue = getAmount();
        confirmPurchase(enteredValue);
      }
    }
  }else{
   digitalWrite(11, LOW);
     analogWrite(R, 0); //Ligando as portas PWM com valor aleatório de duty cicle
    analogWrite(G, 0);
  }
}


